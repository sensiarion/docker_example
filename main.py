import os

import flask
import sqlalchemy
from sqlalchemy import text

app = flask.Flask(__name__)

engine = sqlalchemy.create_engine(
    f"postgresql://{os.environ['DB_USER']}:{os.environ['DB_PASSWORD']}"
    f"@{os.environ['DB_HOST']}:{os.environ['DB_PORT']}/{os.environ['DB_NAME']}"
)


# engine = sqlalchemy.create_engine(
#     "postgresql://postgres:postgres@localhost:4873/postgres"
# )


@app.route('/')
def hello():
    connect = engine.connect()
    users = connect.execute(text("SELECT * FROM users")).all()
    return flask.jsonify({'users_count': len(users)})


@app.route('/user')
def create_user():
    connect = engine.connect()
    connect.execute(
        text("INSERT INTO users (name) VALUES (:name)"),
        parameters=dict(name='pepa')
    )
    connect.commit()
    return flask.jsonify({'status': 'ok'})


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=int(os.environ['PORT']))
